/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule
} from '@nebular/theme';
import { RecursosModule } from './pages/planes-de-emergencia/recursos/recursos.module';
import { ExtintoresModule } from './pages/planes-de-emergencia/recursos/extintores/extintores.module';
import { RedesModule } from './pages/planes-de-emergencia/recursos/redes/redes.module';
import { ViasModule } from './pages/planes-de-emergencia/recursos/vias/vias.module';
import { OperacionesModule } from './pages/planes-de-emergencia/operaciones/operaciones.module';
import { ZonasSeguridadModule } from './pages/planes-de-emergencia/operaciones/zonas-seguridad/zonas-seguridad.module';
import { EscalerasModule } from './pages/planes-de-emergencia/operaciones/escaleras/escaleras.module';
import { CaudalNecesarioModule } from './pages/planes-de-emergencia/operaciones/caudal-necesario/caudal-necesario.module';
import { TiposEmergenciasModule } from './pages/planes-de-emergencia/operaciones/tipos-emergencias/tipos-emergencias.module';
import { ControlSegunTipoModule } from './pages/planes-de-emergencia/operaciones/control-segun-tipo/control-segun-tipo.module';
import { CalderasModule } from './pages/planes-de-emergencia/operaciones/calderas/calderas.module';
import { AzoteasModule } from './pages/planes-de-emergencia/operaciones/azoteas/azoteas.module';
import { PropagacionModule } from './pages/planes-de-emergencia/operaciones/propagacion/propagacion.module';
import { SeguridadModule } from './pages/planes-de-emergencia/seguridad/seguridad.module';
import { MaterialesPeligrososModule } from './pages/planes-de-emergencia/seguridad/materiales-peligrosos/materiales-peligrosos.module';
import { PerimetroSeguridadModule } from './pages/planes-de-emergencia/seguridad/perimetro-seguridad/perimetro-seguridad.module';
import { TableroElectricoModule } from './pages/planes-de-emergencia/seguridad/tablero-electrico/tablero-electrico.module';
import { AscensoresModule } from './pages/planes-de-emergencia/seguridad/ascensores/ascensores.module';
import { GeneradoresModule } from './pages/planes-de-emergencia/seguridad/generadores/generadores.module';
import { ZonasGeneradorModule } from './pages/planes-de-emergencia/seguridad/zonas-generador/zonas-generador.module';
import {CarrosModule} from './pages/planes-de-emergencia/recursos/carros/carros.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    RecursosModule,
    ExtintoresModule,
    RedesModule,
    ViasModule,
    OperacionesModule,
    ZonasSeguridadModule,
    EscalerasModule,
    CaudalNecesarioModule,
    TiposEmergenciasModule,
    ControlSegunTipoModule,
    CalderasModule,
    AzoteasModule,
    PropagacionModule,
    SeguridadModule,
    MaterialesPeligrososModule,
    PerimetroSeguridadModule,
    TableroElectricoModule,
    AscensoresModule,
    GeneradoresModule,
    ZonasGeneradorModule,
    CarrosModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
