export interface Configuracion {
    idUsuario?: string;
    notificaciones?: boolean;
    sonido?: boolean;
    tooltips?: boolean;
    asistente?: boolean;
  }