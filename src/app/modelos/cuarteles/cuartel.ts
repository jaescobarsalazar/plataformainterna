export interface Cuartel {
    id?: string;
    nombre?: string;
    direccion?: string;
    ciudad?: string;
    fono?: number;
    email?: string;
    ultimaActualizacion?: string;
  }