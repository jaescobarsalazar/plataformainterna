export interface Empresa {
    id?: string;
    nombre?: string;
    direccion?: string;
    rut?: string;
    fono?: number;
    email?: string;
    encargado?: string;
    ciudad?: string;
    descripcion?: string;
    suscripcion?: string;
    ultimaActualizacion?: string;
  }
