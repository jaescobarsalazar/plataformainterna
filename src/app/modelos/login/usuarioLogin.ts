export interface Bombero {
    id?: string;
    nombre?: boolean;
    apellidos?: boolean;
    rut?: boolean;
    imagen?: boolean;
    email?: boolean;
    rol?: boolean;
  }