export interface Planta {
  id?: string;
  nombre?: string;
}
export interface Sector {
  id?: string;
  nombre?: string;
}

export interface Ubicacion {
  id?: string;
  nombre?: string;
  cordenadax?: string;
  cordenadaY?: string;
}
