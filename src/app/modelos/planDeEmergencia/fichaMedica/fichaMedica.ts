export interface FichaMedica {
  Id?: string;
  IdTrabajador?: boolean;
  ResumenMedico?: string;
  ResumenMedicamentos?: string;
  ResumenAlergias?: string;
  ResumenOperaciones?: boolean;
}
