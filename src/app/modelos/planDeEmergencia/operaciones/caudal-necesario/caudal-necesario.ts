export interface CaudalNecesario {
  id?: string;
  planta?: string;
  sector?: string;
  ubicacion?: string;
  ubicacionX?: number;
  ubicacionY?: string;
  codigo?: string;
  cantidad?:string;
}
