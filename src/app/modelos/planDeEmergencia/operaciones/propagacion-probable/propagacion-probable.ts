export interface PropagacionProbable {
  id?: string;
  planta?: string;
  sector?: string;
  ubicacion?: string;
  ubicacionX?: number;
  ubicacionY?: string;
  codigo?: string;
  descripcion?: string;
}
