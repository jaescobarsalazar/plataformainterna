export interface ZonaSeguridad {
  id?: string;
  planta?: string;
  sector?: string;
  ubicacion?: string;
  ubicacionX?: number;
  ubicacionY?: string;
  codigo?: string;
}
