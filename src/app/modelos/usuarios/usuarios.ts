export interface Usuario {
    cuartel: string;
    descripcion: any;
    ciudad: any;
    direccion: any;
    suscripcion?: any;
    id?: string;
    nombre?: string;
    apellidos?: string;
    rut?: string;
    fono?: boolean;
    email?: string;
    pass?: boolean;
  }