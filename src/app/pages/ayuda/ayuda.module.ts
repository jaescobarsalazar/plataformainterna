import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AyudaRoutingModule } from './ayuda-routing.module';
import { AyudaComponent } from './ayuda.component';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
import { ListarComponent } from './listar/listar.component';


@NgModule({
  declarations: [AyudaComponent, PanelPrincipalComponent, ListarComponent],
  imports: [
    CommonModule,
    AyudaRoutingModule
  ]
})
export class AyudaModule { }
