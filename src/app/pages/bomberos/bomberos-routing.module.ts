import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarComponent } from './agregar/agregar.component';
import { BomberosComponent } from './bomberos.component';
import { ListarComponent } from './listar/listar.component';
import { ModificarComponent } from './modificar/modificar.component';
import { ReportesComponent } from './reportes/reportes.component';

const routes: Routes = [
  {
    path: '',
    component: BomberosComponent,
    children: [
      {
        path: 'listar',
        component: ListarComponent,
      },
      
      {
        path: 'agregar',
        component: AgregarComponent,
      },
      {
        path: 'modificar',
        component: ModificarComponent,
      },
      {
        path: 'reportes',
        component: ReportesComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BomberosRoutingModule { }
