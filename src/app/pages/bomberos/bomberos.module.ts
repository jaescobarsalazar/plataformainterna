import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BomberosRoutingModule } from './bomberos-routing.module';
import { BomberosComponent } from './bomberos.component';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
import { ListarComponent } from './listar/listar.component';
import { NbActionsModule, NbButtonModule, NbCardModule, NbCheckboxModule, NbDatepickerModule, NbIconModule, NbInputModule, NbRadioModule, 
  NbRouteTabsetModule, NbSearchModule, NbSelectModule, NbTabsetModule, NbToastrModule, NbUserModule } from '@nebular/theme';
import { AgregarComponent } from './agregar/agregar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from 'app/@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { ModificarComponent } from './modificar/modificar.component';
import { ReportesComponent } from './reportes/reportes.component';

@NgModule({
  declarations: [BomberosComponent, PanelPrincipalComponent, ListarComponent, AgregarComponent, ModificarComponent, ReportesComponent],
  imports: [
    CommonModule,
    BomberosRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbSearchModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    NbEvaIconsModule,
    NbToastrModule.forRoot(),
    ngFormsModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    ReactiveFormsModule
  ]
})
export class BomberosModule { }
