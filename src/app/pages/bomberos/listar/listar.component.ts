import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Bombero } from '../../../modelos/bomberos/bomberos';
import { SmartTableData } from 'app/@core/data/smart-table';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbSearchService, NbToastrService, NbWindowService } from '@nebular/theme';
import { BomberosService } from 'app/servicios/bomberos/bomberos.services';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {
  bomberos : Bombero[] ;
  value:any;
  bomberosFiltradas : Bombero[] ;
  bomberosBusqueda : Bombero[];
  edicion: boolean = false;
  bomberoEnEdicion: Bombero;
  index : number = 0;
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  submitted = false;
  constructor(public bomberosServices: BomberosService,
    private service: SmartTableData,
    private router: Router,
    private windowService: NbWindowService,
    private searchService: NbSearchService,
    private toastrService: NbToastrService,
    private route:Router) {
      const data = this.service.getData();
      this.source.load(this.datosPrueba);
      this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        this.value = data.term;
        this.filtro(this.value);
      })

  }
  showToast(position, status, mensaje) {
    this.index += 1;
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { position, status });
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Window content from template',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  ngOnInit() {
    this.bomberosServices.obtenerBomberos().subscribe(datos => {
      this.bomberos = datos;
      this.bomberosBusqueda=datos;
      this.source.load(this.bomberos);
    });
  }
filtro(x){
var contenidoFiltro=x
console.log(contenidoFiltro)
contenidoFiltro=contenidoFiltro.toLocaleLowerCase()
  if(contenidoFiltro.includes("todas")){
    this.bomberos=this.bomberosBusqueda;
  }
  else if(contenidoFiltro!="")
  {
    this.bomberos=this.bomberosBusqueda.filter(bombero => bombero.nombre.toLocaleLowerCase().includes(contenidoFiltro));

  } else if(contenidoFiltro==""){
    this.bomberos=this.bomberosBusqueda;
  }
}

settings = {
  mode: 'internal',
  hideSubHeader: true,
  actions: false,
  sort: false,
  filter: false,
  editable: true,
  columns: {
    nombre: {
      title: 'Nombre',
      type: 'string',
    },
    rut: {
      title: 'Rut',
      type: 'string',
    },
    direccion: {
      title: 'Direccion',
      type: 'string',
    },
    email: {
      title: 'email',
      type: 'string',
    },
    fono: {
      title: 'fono',
      type: 'string',
    },
    encargado: {
      title: 'Encargado',
      type: 'string',
    },
    suscripcion: {
      title: 'Suscripcion',
      type: 'string',
    },
  }
}
datosPrueba = [];
source: LocalDataSource = new LocalDataSource();
styleTable=""
cargarPlanEmergencia(id,nombre)
{

  if (window.confirm('Desea cargar el plan de emergencia?')) {
    //call to remote api, remember that you have to await this
    this.router.navigate(['pages/planes'],
    { queryParams: {
      idEmpresa:id,
      nombreEmpresa:nombre
    } });
  }
}
onEditConfirm(event)
{
  if (window.confirm('Are you sure you want to save?')) {
    //call to remote api, remember that you have to await this
    event.confirm.resolve(event.newData);
  } else {
    event.confirm.reject();
  }
}
onDeleteConfirm(event) {
  console.log(event)
  this.bomberoEnEdicion =event
  if (window.confirm('Está seguro que desea borrar el bombero?')) {
  this.bomberosServices.borrarBombero(this.bomberoEnEdicion);
  event.confirm.resolve(event.newData);
  }
}
onCreateConfirm(event) {
    let d = event.newData
    this.bomberoEnEdicion =d
    this.bomberosServices.agregarBombero(this.bomberoEnEdicion);
    event.confirm.resolve(event.newData);
  return true;
}



}
