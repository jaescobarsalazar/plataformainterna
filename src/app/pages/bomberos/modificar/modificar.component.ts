import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Bombero } from 'app/modelos/bomberos/bomberos';
import { BomberosService } from 'app/servicios/bomberos/bomberos.services';
@Component({
  selector: 'ngx-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.scss']
})
export class ModificarComponent implements OnInit {

  formulario: FormGroup
  bomberos: Bombero={
    nombre:"",
    direccion:"",
    rut:"",
    ciudad:"",
    email:"",
    descripcion:"",
    cuartel:"",
    
  } ;
  constructor(private fb: FormBuilder,
    public bomberosServices: BomberosService,
    private router: Router) { }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: new FormControl(''),
      direccion: new FormControl(''),
      rut: new FormControl(''),
      ciudad: new FormControl(''),
      fono: new FormControl(''),
      email: new FormControl(''),
      descripcion: new FormControl(''),
      cuartel: new FormControl('')
    })
  }
resetForm(){
  this.formulario.reset(this.formulario.value);
}
  doSomething(){
    this.bomberos.nombre=this.formulario.controls.nombre.value;
    this.bomberos.direccion=this.formulario.controls.direccion.value;
    this.bomberos.rut=this.formulario.controls.rut.value;
    this.bomberos.ciudad=this.formulario.controls.ciudad.value;
    this.bomberos.email=this.formulario.controls.email.value;
    this.bomberos.descripcion=this.formulario.controls.descripcion.value;
    this.bomberos.cuartel=this.formulario.controls.cuartel.value;
    this.bomberosServices.agregarBombero(this.bomberos);
    this.router.navigate(['pages/bomberos']);
}
}
