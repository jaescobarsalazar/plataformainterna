import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfiguracionComponent } from './configuracion.component';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
const routes: Routes = [{
  path: '',
  component: ConfiguracionComponent,
  children: [
    {
      path: 'principal',
      component: PanelPrincipalComponent
    },
    {
      path: '',
      component: PanelPrincipalComponent
    },
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionRoutingModule { }

