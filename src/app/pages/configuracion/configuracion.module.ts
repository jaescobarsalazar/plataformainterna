import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionRoutingModule } from './configuracion-routing.module';
import { ConfiguracionComponent } from './configuracion.component';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
import {  NbCardModule, NbCheckboxModule, NbInputModule } from '@nebular/theme';


@NgModule({
  declarations: [ConfiguracionComponent, PanelPrincipalComponent],
  imports: [
    CommonModule,
    ConfiguracionRoutingModule,
   NbCardModule,
   NbInputModule,
   NbCheckboxModule
  ]
})
export class ConfiguracionModule { }
