import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarComponent } from './agregar/agregar.component';
import { CuartelesComponent } from './cuarteles.component';
import { ListadoComponent } from './listado/listado.component';


const routes: Routes = [
  {
    path: '',
    component: CuartelesComponent,
    children: [
      {
        path: '',
        component: ListadoComponent,
      },
      {
        path: 'agregar',
        component: AgregarComponent,
      },

    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuartelesRoutingModule { }
