import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CuartelesRoutingModule } from './cuarteles-routing.module';
import { CuartelesComponent } from './cuarteles.component';
import { ListadoComponent } from './listado/listado.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';
import { AgregarComponent } from './agregar/agregar.component';


@NgModule({
  declarations: [CuartelesComponent, ListadoComponent,AgregarComponent],
  imports: [
    CommonModule,
    CuartelesRoutingModule,
    Ng2SmartTableModule,
    NbCardModule
  ]
})
export class CuartelesModule { }
