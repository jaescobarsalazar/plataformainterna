import { Component, OnInit } from '@angular/core';
import { EmpresasService } from '../../../servicios/empresas/empresas.services';
import { Cuartel } from '../../../modelos/cuarteles/cuartel';
import { SmartTableData } from 'app/@core/data/smart-table';
import { LocalDataSource } from 'ng2-smart-table';
import { CuartelesService } from 'app/servicios/empresas/cuarteles/cuarteles.services';
@Component({
  selector: 'ngx-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {
  cuarteles : Cuartel[] ;
  cuartelesBusqueda : Cuartel[];
  edicion: boolean = false;
  cuartelEnEdicion: Cuartel;
  submitted = false;
  constructor(public cuartelService: CuartelesService,
    private service: SmartTableData) {
      const data = this.service.getData();
      this.source.load(this.datosPrueba);

  }

  ngOnInit() {
    this.cuartelService.obtenerCuarteles().subscribe(datos => {
      this.cuarteles = datos;
      this.cuartelesBusqueda=datos;
      this.source.load(this.cuarteles);
    });
  }
  funct(){
    console.log("here")
  }
settings = {
  attr: {
    class: 'table table-bordered',
    style:"font-size:300px;"
  },
  mode: 'internal',
  actions: {
    edit: true,
    add: true,
    delete: true
  },
  sort: false,
  filter: false,
  editable: true,
  add: {
    addButtonContent: '<i class="nb-plus"></i>',
    createButtonContent: '<i class="nb-checkmark"></i>',
    cancelButtonContent: '<i class="nb-close"></i>',
    confirmCreate: true
  },
  edit: {
    editButtonContent: '<i class="nb-edit"></i>',
    saveButtonContent: '<i class="nb-checkmark"></i>',
    cancelButtonContent: '<i class="nb-close"></i>',
    confirmSave: true
  },
  delete: {
    deleteButtonContent: '<i class="nb-trash"></i>',
    confirmDelete: true,
  },
  columns: {
    nombre: {
      title: 'Nombre',
      type: 'string',
    },
    ciudad: {
      title: 'Ciudad',
      type: 'string',
    },
    direccion: {
      title: 'Direccion',
      type: 'string',
    },
    email: {
      title: 'email',
      type: 'string',
    },
    fono: {
      title: 'fono',
      type: 'string',

    },

  }
}
datosPrueba = [];
source: LocalDataSource = new LocalDataSource();

onEditConfirm(event)
{
  if (window.confirm('Are you sure you want to save?')) {
    //call to remote api, remember that you have to await this
    event.confirm.resolve(event.newData);
  } else {
    event.confirm.reject();
  }
}

onDeleteConfirm(event) {
  console.log(event)
  this.cuartelEnEdicion =event.data
  this.cuartelService.borrarCuartel(this.cuartelEnEdicion);
  event.confirm.resolve(event.newData);
}
onCreateConfirm(event) {
    let d = event.newData
    this.cuartelEnEdicion =d
    this.cuartelService.agregarCuartel(this.cuartelEnEdicion);
    event.confirm.resolve(event.newData);
  return true;
}



}
