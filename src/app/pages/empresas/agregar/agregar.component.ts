import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Empresa } from 'app/modelos/empresas/empresas';
import { EmpresasService } from 'app/servicios/empresas/empresas.services';
import { EmpresasComponent } from '../empresas.component';

@Component({
  selector: 'ngx-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {
  formulario: FormGroup
  empresa : Empresa={
    nombre:"",
    direccion:"",
    rut:"",
    ciudad:"",
    email:"",
    descripcion:"",
    suscripcion:"",
  } ;
  constructor(private fb: FormBuilder,
    public empresasServices: EmpresasService,
    private router: Router) { }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: new FormControl(''),
      direccion: new FormControl(''),
      rut: new FormControl(''),
      ciudad: new FormControl(''),
      fono: new FormControl(''),
      email: new FormControl(''),
      descripcion: new FormControl(''),
      suscripcion: new FormControl('')
    })
  }
resetForm(){
  this.formulario.reset(this.formulario.value);
}
  doSomething(){
    this.empresa.nombre=this.formulario.controls.nombre.value;
    this.empresa.direccion=this.formulario.controls.direccion.value;
    this.empresa.rut=this.formulario.controls.rut.value;
    this.empresa.ciudad=this.formulario.controls.ciudad.value;
    this.empresa.email=this.formulario.controls.email.value;
    this.empresa.descripcion=this.formulario.controls.descripcion.value;
    this.empresa.suscripcion=this.formulario.controls.suscripcion.value;
    this.empresasServices.agregarEmpresa(this.empresa);
    this.router.navigate(['pages/empresas']);
}
}
