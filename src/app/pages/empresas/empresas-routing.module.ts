import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarComponent } from './agregar/agregar.component';
import { EmpresasComponent } from './empresas.component';
import { ListarComponent } from './listar/listar.component';
import { ModificarComponent } from './modificar/modificar.component';

const routes: Routes = [
  {
    path: '',
    component: EmpresasComponent,
    children: [
      {
        path: 'listar',
        component: ListarComponent,
      },
      {
        path: 'agregar',
        component: AgregarComponent,
      },
      {
        path: 'modificar',
        component: ModificarComponent,
      },
      {
        path: '',
        component: ListarComponent,
      },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

  
})
export class EmpresasRoutingModule { }
