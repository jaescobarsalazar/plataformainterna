import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { EmpresasService } from '../../../servicios/empresas/empresas.services';
import { Empresa } from '../../../modelos/empresas/empresas';
import { SmartTableData } from 'app/@core/data/smart-table';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbSearchService, NbToastrService, NbWindowService } from '@nebular/theme';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {
  empresas : Empresa[] ;
  value:any;
  empresasFiltradas : Empresa[] ;
  empresasBusqueda : Empresa[];
  edicion: boolean = false;
  empresaEnEdicion: Empresa;
  index : number = 0;
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  submitted = false;
  constructor(public empresasServices: EmpresasService,
    private service: SmartTableData,
    private router: Router,
    private windowService: NbWindowService,
    private searchService: NbSearchService,
    private toastrService: NbToastrService,
    private route:Router) {
      const data = this.service.getData();
      this.source.load(this.datosPrueba);
      this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        this.value = data.term;
        this.filtro(this.value);
      })

  }
  showToast(position, status, mensaje) {
    this.index += 1;
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { position, status });
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Window content from template',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  ngOnInit() {
    this.empresasServices.obtenerEmpresas().subscribe(datos => {
      this.empresas = datos;
      this.empresasBusqueda=datos;
      this.source.load(this.empresas);
    });
  }
filtro(x){
var contenidoFiltro=x
console.log(contenidoFiltro)
contenidoFiltro=contenidoFiltro.toLocaleLowerCase()
  if(contenidoFiltro.includes("todas")){
    this.empresas=this.empresasBusqueda;
  }
  else if(contenidoFiltro!="")
  {
    this.empresas=this.empresasBusqueda.filter(empresa => empresa.nombre.toLocaleLowerCase().includes(contenidoFiltro));

  } else if(contenidoFiltro==""){
    this.empresas=this.empresasBusqueda;
  }
}

settings = {
  mode: 'internal',
  hideSubHeader: true,
  actions: false,
  sort: false,
  filter: false,
  editable: true,
  columns: {
    nombre: {
      title: 'Nombre',
      type: 'string',
    },
    rut: {
      title: 'Rut',
      type: 'string',
    },
    direccion: {
      title: 'Direccion',
      type: 'string',
    },
    email: {
      title: 'email',
      type: 'string',
    },
    fono: {
      title: 'fono',
      type: 'string',
    },
    encargado: {
      title: 'Encargado',
      type: 'string',
    },
    suscripcion: {
      title: 'Suscripcion',
      type: 'string',
    },
  }
}
datosPrueba = [];
source: LocalDataSource = new LocalDataSource();
styleTable=""
cargarPlanEmergencia(id,nombre)
{

  if (window.confirm('Desea cargar el plan de emergencia?')) {


    this.router.navigate(['pages/planes/trabajadores'],
    { queryParams: {
      idEmpresa:id,
      nombreEmpresa:nombre
    } });
  }
}
onEditConfirm(event)
{
  if (window.confirm('Are you sure you want to save?')) {

    event.confirm.resolve(event.newData);
  } else {
    event.confirm.reject();
  }
}
onDeleteConfirm(event) {
  console.log(event)
  this.empresaEnEdicion =event
  if (window.confirm('Está seguro que desea borrar la empresa?')) {
  this.empresasServices.borrarEmpresa(this.empresaEnEdicion);
  event.confirm.resolve(event.newData);
  }
}
onCreateConfirm(event) {
    let d = event.newData
    this.empresaEnEdicion =d
    this.empresasServices.agregarEmpresa(this.empresaEnEdicion);
    event.confirm.resolve(event.newData);
  return true;
}



}
