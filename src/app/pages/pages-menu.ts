import { NbMenuItem, NbBadgeModule } from "@nebular/theme";

const rutaOp = "/pages/planes/operaciones/";
const rutaSeg = "/pages/planes/seguridad/";
const rutaRec = "/pages/planes/recursos/";
export var MENU_ITEMS: NbMenuItem[] = [
  {
    title: "Inicio",
    icon: "corner-down-right-outline",
    link: "/pages/resumen",
    home: false,
    children: [
      {
        title: "Dasboard",
        selected: false,
      },
      {
        title: "Resumen",
        selected: false,
      },
    ],
  },
  {
    title: "Planes de Emergencia",
    icon: "layers-outline",
    expanded: false,
    hidden: false,
    link: "/pages/planes",
    children: [
      {
        title: "RRHH",
        expanded: false,
        children: [
          {
            title: "Trabajadores",
            selected: false,
            link: "/pages/planes/trabajadores",
          },
          {
            title: "FichaMedica",
            selected: false,
          },
        ],
      },
      {
        title: "Operaciones",
        expanded: false,
        children: [
          {
            title: "Red S/H",
            selected: false,
            link: rutaOp + "redes",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "redes/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "redes/agregar",
              }],

          },
          {
            title: "Zonas de Seguridad",
            selected: false,
            link: rutaOp + "zonas-seguridad",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "zonas-seguridad/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "zonas-seguridad/agregar",
              }],
          },
          {
            title: "Vias IN/OUT",
            selected: false,
            link: rutaOp + "vias",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "vias/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "vias/agregar",
              }],
          },
          {
            title: "Escaleras",
            selected: false,
            link: rutaOp + "escaleras",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "escaleras/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "escaleras/agregar",
              }],
          },
          {
            title: "Caudal Necesario",
            selected: false,
            link: rutaOp + "caudal-necesario",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "caudal-necesario/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "caudal-necesario/agregar",
              }],
          },
          {
            title: "Tipos de emergencias",
            selected: false,
            link: rutaOp + "tipos-emergencias",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "tipos-emergencias/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "tipos-emergencias/agregar",
              }],
          },
          {
            title: "Control segun tipo",
            selected: false,
            link: rutaOp + "control-segun-tipo",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "control-segun-tipo/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "control-segun-tipo/agregar",
              }],
          },
          {
            title: "Calderas",
            selected: false,
            link: rutaOp + "calderas",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "calderas/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "calderas/agregar",
              }],
          },
          {
            title: "Azoteas",
            selected: false,
            link: rutaOp + "azoteas",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "azoteas/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "azoteas/agregar",
              }],
          },
          {
            title: "Sis. No convencional",
            selected: false,
            link: rutaOp + "no-convencionales",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "no-convencionales/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "no-convencionales/agregar",
              }],
          },
          {
            title: "Propagación probable",
            selected: false,
            link: rutaOp + "propagacion",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaOp + "propagacion/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaOp + "propagacion/agregar",
              }],
          },
        ],
      },
      {
        title: "Seguridad",
        expanded: false,
        children: [
          {
            title: "Materiales Peligrosos",
            selected: false,
            link: rutaSeg+"materiales-peligrosos/",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "materiales-peligrosos/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "materiales-peligrosos/agregar",
              }],
          },
          {
            title: "Vias in/OUT",
            selected: false,
            link: rutaSeg+"vias",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "vias/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "vias/agregar",
              }],
          },
          {
            title: "Perimetros de seguridad",
            selected: false,
            link: rutaSeg+"perimetro-seguridad",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "perimetro-seguridad/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "perimetro-seguridad/agregar",
              }],
          },
          {
            title: "Tablero Electrico",
            selected: false,
            link: rutaSeg+"tablero-electrico",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "tablero-electrico/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "tablero-electrico/agregar",
              }],
          },
          {
            title: "Ascensores",
            selected: false,
            link: rutaSeg+"ascensores",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "ascensores/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "ascensores/agregar",
              }],
          },
          {
            title: "Generadores",
            selected: false,
            link: rutaSeg+"generadores",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "generadores/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "generadores/agregar",
              }],
          },
          {
            title: "Zonas Generador",
            selected: false,
            link: rutaSeg+"zonas-generador",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaSeg + "zonas-generador/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaSeg + "zonas-generador/agregar",
              }],
          },
        ],
      },
      {
        title: "Recursos",
        expanded: false,
        children: [
          {
            title: "Extintores",
            selected: false,
            link: rutaRec + "extintores",
            children: [
            {
              title: "Listado",
              selected: false,
              link: rutaRec + "extintores/",
            },
            {
              title: "Crear",
              selected: false,
              link: rutaRec + "extintores/agregar",
            }],

          },
          {
            title: "Carros",
            selected: false,
            link: rutaRec + "carros",
            children: [
              {
                title: "Listado",
                selected: false,
                link: rutaRec + "carros/",
              },
              {
                title: "Crear",
                selected: false,
                link: rutaRec + "carros/agregar",
              }],
          },
          {
            title: "Cuarteles cercanos",
            selected: false,
          },
          {
            title: "Helipuerto",
            selected: false,
          },
        ],
      },
      {
        title: "Planos",
        expanded: false,
        children: [
          {
            title: "Plantas",
            selected: false,
          },
          {
            title: "Sectores",
            selected: false,
          },
          {
            title: "Habitaciones",
            selected: false,
          },
          {
            title: "Iconos",
            selected: false,
          },
          {
            title: "Configuracion",
            selected: false,
          },
        ],
      },
    ],
  },

  {
    title: "Usuarios",
    icon: "person-outline",
    link: "/pages/usuarios",
    children: [
      {
        title: 'Listado',
        selected:false,
        link: '/pages/usuarios',
      },
      {
        title: 'Agregar',
        selected:false,
        link: '/pages/usuarios/agregar',
      },
      {
        title: 'Roles',
        selected:false,
        link: '/pages/usuarios',
      },
      {
        title: "Listado",
        selected: false,
      },
      {
        title: "Configuracion",
        selected: false,
      },
    ],
  },
  {
    title: "Empresas",
    icon: "home-outline",
    link: "/pages/empresas",
    expanded: false,
    children: [
      {
        title: "Listado",
        selected: false,
        link: "/pages/empresas",
      },
      {
        title: "Agregar",
        selected: false,
        link: "/pages/empresas/agregar",
      },
    ],
  },
  {
    title: "Cuarteles",
    icon: "pantone-outline",
    link: "/pages/cuarteles",
    home: false,
    children: [
      {
        title: "Listado",
        selected: false,
        link: "/pages/cuarteles",
      },
      {
        title: "Agregar",
        selected: false,
        link: "/pages/cuarteles/agregar",
      },
    ],
  },
  {
    title: "Comandantes",
    icon: "people-outline",
    link: "/pages/bomberos",
    home: false,
    children: [
      {
        title: 'Listado',
        selected:false,
        link: '/pages/bomberos/listar',
      },
      {
        title: 'Agregar',
        selected:false,
        link: '/pages/bomberos/agregar',  
      },
      {
        title: 'Modificar',
        selected:false,
        link: '/pages/bomberos/modificar',  
      },
      {
        title: 'Reportes',
        selected:false,
        link:'/pages/bomberos/reportes',  
      },
      {
        title: "Listado",
        selected: false,
        link: "/pages/bomberos",
      },
      {
        title: "Agregar",
        selected: false,
      },
      {
        title: "Reportes",
        selected: false,
      },
      {
        title: "Configuracion",
        selected: false,

      },
    ],
  },
  {
    title: "Reportes",
    icon: "bar-chart-outline",
    link: "/pages/reportes",
    home: false,
    children: [
      {
        title: "Empresas",
        selected: false,
      },
      {
        title: "Catastrofes",
        selected: false,
      },
    ],
  },
  {
    title: "Configuracion",
    icon: "options-2-outline",
    link: "/pages/configuracion",
    home: false,
  },

  {
    title: "Chat",
    icon: "question-mark-circle-outline",
    link: "/pages/ayuda",
    home: false,
  },
  {
    title: "Ayuda",
    icon: "question-mark-circle-outline",
    link: "/pages/ayuda",
    home: false,
  },
];
