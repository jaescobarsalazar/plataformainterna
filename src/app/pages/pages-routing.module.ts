import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'usuarios',
      loadChildren: () => import('./usuarios/usuarios.module')
        .then(m => m.UsuariosModule)
    },
    {
      path: '',
      loadChildren: () => import('./empresas/empresas.module')
        .then(m => m.EmpresasModule)
    },
    {
      path: 'empresas',
      loadChildren: () => import('./empresas/empresas.module')
        .then(m => m.EmpresasModule)
    },
    {
      path: 'resumen',
      loadChildren: () => import('./resumen/resumen.module')
        .then(m => m.ResumenModule)
    },
    {
      path: 'planes',
      loadChildren: () => import('./planes-de-emergencia/planes-de-emergencia.module')
        .then(m => m.PlanesDeEmergenciaModule)
    },
    {
      path: 'cuarteles',
      loadChildren: () => import('./cuarteles/cuarteles.module')
        .then(m => m.CuartelesModule)
    },
    {
      path: 'bomberos',
      loadChildren: () => import('./bomberos/bomberos.module')
        .then(m => m.BomberosModule)
    },
    {
      path: 'configuracion',
      loadChildren: () => import('./configuracion/configuracion.module')
        .then(m => m.ConfiguracionModule)
    },
    {
      path: 'ayuda',
      loadChildren: () => import('./ayuda/ayuda.module')
        .then(m => m.AyudaModule)
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
