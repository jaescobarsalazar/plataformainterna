import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtintoresComponent } from './extintores.component';

describe('ExtintoresComponent', () => {
  let component: ExtintoresComponent;
  let fixture: ComponentFixture<ExtintoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtintoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtintoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
