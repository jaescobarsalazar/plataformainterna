import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtintoresRoutingModule } from './extintores-routing.module';
import { ExtintoresComponent } from './extintores.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';


@NgModule({
  declarations: [ExtintoresComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    ExtintoresRoutingModule
  ]
})
export class ExtintoresModule { }
