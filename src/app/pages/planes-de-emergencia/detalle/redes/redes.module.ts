import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RedesRoutingModule } from './redes-routing.module';
import { RedesComponent } from './redes.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';


@NgModule({
  declarations: [RedesComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    RedesRoutingModule
  ]
})
export class RedesModule { }
