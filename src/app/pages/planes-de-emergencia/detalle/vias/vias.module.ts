import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViasRoutingModule } from './vias-routing.module';
import { ViasComponent } from './vias.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';


@NgModule({
  declarations: [ViasComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    ViasRoutingModule
  ]
})
export class ViasModule { }
