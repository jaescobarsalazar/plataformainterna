import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AzoteasComponent } from './azoteas.component';

describe('AzoteasComponent', () => {
  let component: AzoteasComponent;
  let fixture: ComponentFixture<AzoteasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AzoteasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AzoteasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
