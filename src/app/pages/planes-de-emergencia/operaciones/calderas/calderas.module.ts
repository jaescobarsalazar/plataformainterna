import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalderasRoutingModule } from './calderas-routing.module';
import { CalderasComponent } from './calderas.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';
@NgModule({
  declarations: [CalderasComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    CalderasRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    Ng2SmartTableModule,
    TablesRoutingModule
  ]
})
export class CalderasModule { }
