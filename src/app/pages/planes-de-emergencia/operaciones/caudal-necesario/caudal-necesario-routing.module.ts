import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { CaudalNecesarioComponent } from './caudal-necesario.component';
import { AgregarComponent } from './agregar/agregar.component';
const routes: Routes = [ {
  path: '',
  component: CaudalNecesarioComponent,
  children: [
    {
      path: '',
      component: ListarComponent
    },
    {
      path: 'listar',
      component: ListarComponent
    },
    {
      path: 'agregar',
      component: AgregarComponent
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaudalNecesarioRoutingModule { }
