import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaudalNecesarioComponent } from './caudal-necesario.component';

describe('CaudalNecesarioComponent', () => {
  let component: CaudalNecesarioComponent;
  let fixture: ComponentFixture<CaudalNecesarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaudalNecesarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaudalNecesarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
