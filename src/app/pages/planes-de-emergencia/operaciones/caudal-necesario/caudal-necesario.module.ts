import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaudalNecesarioRoutingModule } from './caudal-necesario-routing.module';
import { CaudalNecesarioComponent } from './caudal-necesario.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';


@NgModule({
  declarations: [CaudalNecesarioComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    CaudalNecesarioRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    Ng2SmartTableModule,
    TablesRoutingModule
  ]
})
export class CaudalNecesarioModule { }
