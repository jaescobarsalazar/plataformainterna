import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { ControlSegunTipoComponent } from './control-segun-tipo.component';
import { AgregarComponent } from './agregar/agregar.component';
const routes: Routes = [ {
  path: '',
  component: ControlSegunTipoComponent,
  children: [
    {
      path: '',
      component: ListarComponent
    },
    {
      path: 'listar',
      component: ListarComponent
    },
    {
      path: 'agregar',
      component: AgregarComponent
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ControlSegunTipoRoutingModule { }
