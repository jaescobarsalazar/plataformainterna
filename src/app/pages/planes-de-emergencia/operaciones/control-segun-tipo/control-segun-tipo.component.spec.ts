import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlSegunTipoComponent } from './control-segun-tipo.component';

describe('ControlSegunTipoComponent', () => {
  let component: ControlSegunTipoComponent;
  let fixture: ComponentFixture<ControlSegunTipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlSegunTipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlSegunTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
