import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';
import { ControlSegunTipoRoutingModule } from './control-segun-tipo-routing.module';
import { ControlSegunTipoComponent } from './control-segun-tipo.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';

@NgModule({
  declarations: [ControlSegunTipoComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    ControlSegunTipoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    Ng2SmartTableModule,
    TablesRoutingModule
  ]
})
export class ControlSegunTipoModule { }
