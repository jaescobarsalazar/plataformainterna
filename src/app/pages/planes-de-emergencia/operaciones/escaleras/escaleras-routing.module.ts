import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EscalerasComponent } from './escaleras.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';

const routes: Routes = [ {
  path: '',
  component: EscalerasComponent,
  children: [
    {
      path: '',
      component: ListarComponent
    },
    {
      path: 'listar',
      component: ListarComponent
    },
    {
      path: 'agregar',
      component: AgregarComponent
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EscalerasRoutingModule { }

