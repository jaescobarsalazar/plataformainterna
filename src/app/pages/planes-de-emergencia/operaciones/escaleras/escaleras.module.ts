import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { EscalerasRoutingModule } from './escaleras-routing.module';
import { EscalerasComponent } from './escaleras.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';
import { ListarComponent } from './listar/listar.component';

@NgModule({
  declarations: [EscalerasComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    EscalerasRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    Ng2SmartTableModule,
    TablesRoutingModule,
  ]
})
export class EscalerasModule { }
