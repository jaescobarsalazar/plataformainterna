import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperacionesComponent } from './operaciones.component';
const routes: Routes = [
  {
    path: '',
    component: OperacionesComponent,
    children: [
      {
        path: 'redes',
        loadChildren: () => import('./redes/redes.module')
          .then(m => m.RedesModule)
      },
      {
        path: 'azoteas',
        loadChildren: () => import('./azoteas/azoteas.module')
          .then(m => m.AzoteasModule)
      },
      {
        path: 'calderas',
        loadChildren: () => import('./calderas/calderas.module')
          .then(m => m.CalderasModule
            )
      },
      {
        path: 'caudal-necesario',
        loadChildren: () => import('./caudal-necesario/caudal-necesario.module')
          .then(m => m.CaudalNecesarioModule)
      },
      {
        path: 'control-segun-tipo',
        loadChildren: () => import('./control-segun-tipo/control-segun-tipo.module')
          .then(m => m.ControlSegunTipoModule)
      },
      {
        path: 'escaleras',
        loadChildren: () => import('./escaleras/escaleras.module')
          .then(m => m.EscalerasModule)
      },
      {
        path: 'propagacion',
        loadChildren: () => import('./propagacion/propagacion.module')
          .then(m => m.PropagacionModule)
      },
      {
        path: 'tipos-emergencias',
        loadChildren: () => import('./tipos-emergencias/tipos-emergencias.module')
          .then(m => m.TiposEmergenciasModule)
      },
      {
        path: 'vias',
        loadChildren: () => import('./vias/vias.module')
          .then(m => m.ViasModule)
      },
      {
        path: 'zonas-seguridad',
        loadChildren: () => import('./zonas-seguridad/zonas-seguridad.module')
          .then(m => m.ZonasSeguridadModule)
      },
      {
        path: '',
        loadChildren: () => import('./redes/redes.module')
          .then(m => m.RedesModule)
      },
    ],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperacionesRoutingModule { }
