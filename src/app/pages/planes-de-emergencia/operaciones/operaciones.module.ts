import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { OperacionesRoutingModule } from './operaciones-routing.module';
import { OperacionesComponent } from './operaciones.component';
import { AgregarComponent } from './redes/agregar/agregar.component';


@NgModule({
  declarations: [OperacionesComponent],

  imports: [
    CommonModule,
    OperacionesRoutingModule,
    Ng2SmartTableModule,
  ]
})
export class OperacionesModule { }
