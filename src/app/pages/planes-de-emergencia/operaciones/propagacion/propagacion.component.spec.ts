import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropagacionComponent } from './propagacion.component';

describe('PropagacionComponent', () => {
  let component: PropagacionComponent;
  let fixture: ComponentFixture<PropagacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropagacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropagacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
