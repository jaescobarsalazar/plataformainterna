import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PropagacionRoutingModule } from './propagacion-routing.module';
import { PropagacionComponent } from './propagacion.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';

@NgModule({
  declarations: [PropagacionComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    PropagacionRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    Ng2SmartTableModule,
    TablesRoutingModule
  ]
})
export class PropagacionModule { }
