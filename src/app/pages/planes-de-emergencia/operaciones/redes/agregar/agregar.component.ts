import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbComponentStatus, NbDialogRef, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { Planta, Sector, Ubicacion } from 'app/modelos/mapa/mapa';
import { Red } from 'app/modelos/redes/redes';

import { MapaService } from 'app/servicios/mapa/mapa.service';
import { RedesService } from 'app/servicios/redes/redes.service';
@Component({
  selector: 'ngx-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {
  plantas:Planta[];
  sectores:Sector[];
  ubicaciones:Ubicacion[];
  FormularioDatos: FormGroup;
  formularioPlantas: FormGroup;
  formularioSectores: FormGroup;
  formularioUbicaciones: FormGroup;
  status: NbComponentStatus="success";
  statusEliminar: NbComponentStatus="danger";
  tipoRed = "Seca"
  mensaje= { title: null, body: 'Red '+this.tipoRed+' registrada.' }
  mensajeEliminar= { title: null, body: 'Red '+this.tipoRed+' eliminada.' }
  index = 1;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  @Input() title: string;
  @Input() titleEliminar: string;


  contentEliminar = `I'm cool toaster!`;
  content = `I'm cool toaster!`;
  constructor(private fb: FormBuilder,
    public servicioRed: RedesService,
    private toastrService: NbToastrService,
    private mapaService : MapaService) {
     this.formularioPlantas = this.fb.group({
      inputPlanta: ['', [Validators.required]],
    });
    this.formularioSectores = this.fb.group({
      inputSector: ['', [Validators.required]],
    });
    this.formularioUbicaciones = this.fb.group({
      inputUbicacion: ['', [Validators.required]],
    });

    this.FormularioDatos = this.fb.group({
      inputCodigo: ['', [Validators.required]],
      inputTipo: ['', [Validators.required]],
      inputFechaMantencion: ['', [Validators.required]],
    });

 }

 Guardar(fuu){
   var nuevaRed : Red={
     planta:this.formularioPlantas.controls.inputPlanta.value,
     sector:this.formularioSectores.controls.inputSector.value,
     ubicacion:this.formularioUbicaciones.controls.inputUbicacion.value
   };
this.servicioRed.agregarRed(nuevaRed);
console.log("red registrada");
this.crearNotificacion();
fuu();
}

  ngOnInit() {
    this.mapaService.obtenerPlantas().subscribe(datos => {
      this.plantas = datos;
      console.log(this.plantas);
      console.log("estoy cargando las plantas");
    });

    this.mapaService.obtenerUbicaciones().subscribe(datos => {
      this.ubicaciones = datos;
      console.log(this.ubicaciones);
      console.log("estoy cargando las Ubicaciones");
    });

    this.mapaService.obtenerSectores().subscribe(datos => {
      this.sectores = datos;
      console.log(this.sectores);
      console.log("estoy cargando las Sectores");
    });
  }
  crearNotificacion() {
    this.showToast(this.status, this.title, this.content);
  }

  actualizarPlanta(e) {
    this.formularioPlantas.controls.inputPlanta.setValue(e.target.value, {
      onlySelf: true
    })
  }
  actualizarSector(e) {
    this.formularioSectores.controls.inputSector.setValue(e.target.value, {
      onlySelf: true
    })
  }
  actualizarUbicacion(e) {
    this.formularioUbicaciones.controls.inputUbicacion.setValue(e.target.value, {
      onlySelf: true
    })
  }
  validarPlanta() {
    console.log(this.formularioPlantas)
    console.log( this.formularioPlantas.controls.inputPlanta.value)
  }
  validarSector() {
    console.log(this.formularioPlantas)
    console.log( this.formularioPlantas.controls.inputPlanta.value)
  }
  validarDatos() {
    console.log(this.FormularioDatos)
  }

  validarUbicacion() {
    console.log(this.formularioPlantas)
    console.log( this.formularioPlantas.controls.inputPlanta.value)
  }
  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `Toast ${this.index}${titleContent}`,
      config);
  }

}
