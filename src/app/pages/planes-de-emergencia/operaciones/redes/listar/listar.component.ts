import { Component, Input, OnInit } from '@angular/core';
import { NbComponentStatus, NbDialogService, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { Red } from 'app/modelos/redes/redes';
import { RedesService } from 'app/servicios/redes/redes.service';
import { AgregarComponent } from '../agregar/agregar.component';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {
  redes:Red[];
  index = 1;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
    control=true;
  constructor(public servicioRed: RedesService,
    private dialogService: NbDialogService,
    private modalService: NgbModal,
    private toastrService: NbToastrService) {


   /* this.redes=[
      {
        planta:"PLanta1",
        sector:"Recepcion",
        ubicacion:"Cocina",
        codigo:"312433",
        manguera:2,
        },
        {
          planta:"Planta1",
          sector:"Recepcion",
          ubicacion:"Garaje",
          codigo:"554344",
          manguera:1
          },
    ]*/
   }
   closeResult: string;
   position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
   preventDuplicates = false;
   open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  statusEliminar: NbComponentStatus="danger";
  mensajeEliminar= { title: null, body: 'Red  eliminada.' }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  eliminar(red){
    console.log("aaaaaaa")
    this.servicioRed.borrarRed(red);
    this.crearNotificacionEliminar();
  }
  @Input() titleEliminar: string;
  contentEliminar = `Red eliminada!`;
  crearNotificacionEliminar() {
    this.showToast(this.statusEliminar, this.titleEliminar, this.contentEliminar);
  }

mostrarManguera(manguera){
if(manguera==1){
  this.control=true;
}else{
  this.control=false;
}
}
   alerta(item){
    console.log(item);
    console.log(item.planta);
   }
   validar(e) {
    console.log(e)
   }

datos:[any];
  ngOnInit() {

this.servicioRed.obtenerRedes().subscribe(datos => {
  this.redes = datos;
  console.log(this.redes);
  console.log("estoy cargando las redes");
});
  }
  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `Toast ${this.index}${titleContent}`,
      config);
  }
}
