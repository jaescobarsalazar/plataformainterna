import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RedesComponent } from './redes.component';

import { ListarComponent } from './listar/listar.component';

const routes: Routes = [
  {
    path: '',
    component: RedesComponent,
    children: [
      {
        path: '',
        component: ListarComponent
      },
]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedesRoutingModule { }
