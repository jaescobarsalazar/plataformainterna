import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RedesRoutingModule } from './redes-routing.module';
import { RedesComponent } from './redes.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbDialogModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { NgbModalBackdrop } from '@ng-bootstrap/ng-bootstrap/modal/modal-backdrop';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [RedesComponent,ListarComponent,AgregarComponent],
  imports: [
    CommonModule,
    RedesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    NbCardModule,
    NbDialogModule,
    NgbModule
  ],
  entryComponents:[AgregarComponent]

})
export class RedesModule { }
