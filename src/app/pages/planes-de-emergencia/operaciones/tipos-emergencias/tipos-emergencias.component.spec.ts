import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposEmergenciasComponent } from './tipos-emergencias.component';

describe('TiposEmergenciasComponent', () => {
  let component: TiposEmergenciasComponent;
  let fixture: ComponentFixture<TiposEmergenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposEmergenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposEmergenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
