import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiposEmergenciasRoutingModule } from './tipos-emergencias-routing.module';
import { TiposEmergenciasComponent } from './tipos-emergencias.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';

@NgModule({
  declarations: [TiposEmergenciasComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    TiposEmergenciasRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    Ng2SmartTableModule,
    TablesRoutingModule
  ]
})
export class TiposEmergenciasModule { }
