import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ViasRoutingModule } from './vias-routing.module';
import { ViasComponent } from './vias.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { TablesRoutingModule } from 'app/pages/tables/tables-routing.module';

@NgModule({
  declarations: [ViasComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    ViasRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule,
    TablesRoutingModule,
    Ng2SmartTableModule 
  ]
})
export class ViasModule { }
