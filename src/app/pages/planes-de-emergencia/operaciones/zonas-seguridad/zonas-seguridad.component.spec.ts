import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonasSeguridadComponent } from './zonas-seguridad.component';

describe('ZonasSeguridadComponent', () => {
  let component: ZonasSeguridadComponent;
  let fixture: ComponentFixture<ZonasSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonasSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonasSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
