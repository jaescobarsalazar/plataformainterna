import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZonasSeguridadRoutingModule } from './zonas-seguridad-routing.module';
import { ZonasSeguridadComponent } from './zonas-seguridad.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';

@NgModule({
  declarations: [ZonasSeguridadComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    ZonasSeguridadRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  
  ]
})
export class ZonasSeguridadModule { }
