import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { SmartTableData } from "app/@core/data/smart-table";
import { LocalDataSource } from "ng2-smart-table";
import { NbSearchService } from "@nebular/theme";
import { ActivatedRoute, Params } from "@angular/router";
import "rxjs/add/operator/filter";
import { Trabajador } from "app/modelos/planDeEmergencia/trabajadores/trabajador";

@Component({
  selector: "ngx-panel-principal",
  templateUrl: "./panel-principal.component.html",
  styleUrls: ["./panel-principal.component.scss"],
})
export class PanelPrincipalComponent implements OnInit {
  busqueda: string;

  propagar = new EventEmitter<string>();
  divTablaTrabajadores: string;
  rutTrabajador:string;
  divFichaMedica: string;
  estiloFichaMedica: string;
  divListadoCompacto: string;
  controlFiltro: boolean;
  mensaje: string;
  styleTablaTrabajadores: string;
  TablaPrincipal: boolean;
  ListadoCompacto: boolean;
  DatosFichaMedica: boolean;
  settings = {
    mode: "internal",
    actions: {
      edit: true,
      add: true,
      delete: true,
    },
    sort: false,
    filter: false,
    editable: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nombre: {
        title: "Nombre",
        type: "string",
      },
      rut: {
        title: "Rut",
        type: "string",
      },
      cargo: {
        title: "Cargo",
        type: "string",
      },
      ubicacion: {
        title: "Ubicacion",
        type: "string",
      },
      ml: {
        title: "M.L",
        type: "string",
      },
      fono: {
        title: "Cargo",
        type: "string",
      },
      hm: {
        title: "HM",
        type: "string",
      },
    },
  };
  trabajadores: { nombre: string; rut: string }[] = [
    { nombre: "Carla Espinosa", rut: "11.111.111-1" },
    { nombre: "Bob Kelso", rut: "22.222.222.2" },
    { nombre: "Janitor", rut: "33.333.333-3" },
    { nombre: "Perry Cox", rut: "44.444.444-4" },
    { nombre: "Ben Sullivan", rut: "55-555-555-5" },
    { nombre: "Carla Espinosa", rut: "11.111.111-1" },
    { nombre: "Bob Kelso", rut: "22.222.222.2" },
    { nombre: "Janitor", rut: "33.333.333-3" },
    { nombre: "Perry Cox", rut: "44.444.444-4" },
    { nombre: "Ben Sullivan", rut: "55-555-555-5" },
    { nombre: "Carla Espinosa", rut: "11.111.111-1" },
    { nombre: "Bob Kelso", rut: "22.222.222.2" },
    { nombre: "Janitor", rut: "33.333.333-3" },
    { nombre: "Perry Cox", rut: "44.444.444-4" },
    { nombre: "Ben Sullivan", rut: "55-555-555-5" },
    { nombre: "Carla Espinosa", rut: "11.111.111-1" },
    { nombre: "Bob Kelso", rut: "22.222.222.2" },
    { nombre: "Janitor", rut: "33.333.333-3" },
    { nombre: "Perry Cox", rut: "44.444.444-4" },
    { nombre: "Ben Sullivan", rut: "55-555-555-5" },
  ];
  trabajadoresFiltrados: { nombre: string; rut: string }[];

  public cargarFichaMedica(trabajador) {
    console.log(trabajador)
    this.rutTrabajador=trabajador.data.rut;
    console.log(this.rutTrabajador);
    this.TablaPrincipal = false;
    this.ListadoCompacto = true;
    this.DatosFichaMedica = true;
  }
  cargarTablaPrincipal() {
    this.styleTablaTrabajadores = "hidden:false;";
  }
  datosPrueba = [
    {
      nombre: "Prueba",
      rut: "18.492.316-5",
      rol: "OperadorInterno",
      cargo: "Cargo1",
      ubicacion: "PLanta 1",
      ml: "No",
      fono: "93562626",
      hm: "Ver",
    },
    {
      nombre: "Prueba",
      rut: "11.111.111-k",
      rol: "Chofer",
      cargo: "CargoPrueba",
      ubicacion: "Bodega",
      ml: "No",
      fono: "98596895",
      hm: "Ver",
    },
    {
      nombre: "Test",
      rut: "12.235.235.-1",
      rol: "Consultor",
      cargo: "Test",
      ubicacion: "Planta 3",
      ml: "Si",
      fono: "56895978",
      hm: "Ver",
    },
  ];
  source: LocalDataSource = new LocalDataSource();
  value = "Celulosa Arauco";
  nombreEmpresa = "";
  idEmpresa = "";
  constructor(
    private service: SmartTableData,
    private searchService: NbSearchService,
    private route: ActivatedRoute
  ) {
    this.trabajadoresFiltrados = this.trabajadores;
    this.controlFiltro = false;
    this.TablaPrincipal = true;
    this.ListadoCompacto = false;
    this.DatosFichaMedica = false;
    this.divTablaTrabajadores = "col-12";
    this.divFichaMedica = "col-12 col-md-8";
    this.divListadoCompacto = "col-12 col-md-4";
    this.estiloFichaMedica = "hidden:true;";
    const data = this.service.getData();
    this.source.load(this.datosPrueba);
    this.searchService.onSearchSubmit().subscribe((data: any) => {
      this.value = data.term;
    });

    this.route.queryParams.subscribe((params) => {
      this.nombreEmpresa = params["nombreEmpresa"];
      this.idEmpresa = params["idEmpresa"];
    });
  }

  filtrarNombre(event: any) {
    if (event.target.value != "") {
      this.controlFiltro = false;
      var contenidoFiltro = event.target.value.toString();
      this.trabajadoresFiltrados = this.trabajadores.filter((name) =>
        name.nombre.toLocaleLowerCase().includes(contenidoFiltro)
      );
    } else {
      this.controlFiltro = true;
      this.trabajadoresFiltrados = this.trabajadores;
    }
  }
  ngOnInit(): void {

  }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
