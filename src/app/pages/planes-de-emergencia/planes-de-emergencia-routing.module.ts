import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
import { PlanesDeEmergenciaComponent } from './planes-de-emergencia.component';

const routes: Routes = [{
  path: '',
  component: PlanesDeEmergenciaComponent,
  children: [
    {
      path: 'principal/:idEmpresa',
      component: PanelPrincipalComponent
    },
    {
      path: 'trabajadores',
      loadChildren: () => import('./rrhh/rrhh.module')
        .then(m => m.RrhhModule)
    },
    {
      path: 'operaciones',
      loadChildren: () => import('./operaciones/operaciones.module')
        .then(m => m.OperacionesModule)
    },
    {
      path: 'recursos',
      loadChildren: () => import('./recursos/recursos.module')
        .then(m => m.RecursosModule)
    },
    {
      path: 'seguridad',
      loadChildren: () => import('./seguridad/seguridad.module')
        .then(m => m.SeguridadModule)
    },

  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanesDeEmergenciaRoutingModule { }
