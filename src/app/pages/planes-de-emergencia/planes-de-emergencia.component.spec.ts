import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanesDeEmergenciaComponent } from './planes-de-emergencia.component';

describe('PlanesDeEmergenciaComponent', () => {
  let component: PlanesDeEmergenciaComponent;
  let fixture: ComponentFixture<PlanesDeEmergenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanesDeEmergenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanesDeEmergenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
