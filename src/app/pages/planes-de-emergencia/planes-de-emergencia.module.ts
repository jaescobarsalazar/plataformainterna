import { NgModule } from '@angular/core';

import { PlanesDeEmergenciaRoutingModule } from './planes-de-emergencia-routing.module';
import { PlanesDeEmergenciaComponent } from './planes-de-emergencia.component';
import { NbCardModule, NbCheckboxModule, NbInputModule, NbListModule, NbRouteTabsetModule, NbSearchModule, NbTabsetModule, NbTooltipModule, NbUserModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ListarPersonalComponent } from './rrhh/listar-personal/listar-personal.component';
import { PlantasComponent } from './planos/plantas/plantas.component';
import { TooltipComponent } from '../modal-overlays/tooltip/tooltip.component';
import { PruebaModule } from '../prueba/prueba.module';
import { CommonModule } from '@angular/common';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';




@NgModule({
  declarations: [
    PlanesDeEmergenciaComponent,
    ListarPersonalComponent,
    PlantasComponent,
  PanelPrincipalComponent],
  imports: [
    NbCardModule,
    Ng2SmartTableModule,
    PlanesDeEmergenciaRoutingModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbSearchModule,
    NbCheckboxModule,
    NbInputModule,
    NbTooltipModule,
    PruebaModule,
    CommonModule,
    NbUserModule,
    NbListModule
  ],
  entryComponents: [
    TooltipComponent
  ],
})
export class PlanesDeEmergenciaModule { }
