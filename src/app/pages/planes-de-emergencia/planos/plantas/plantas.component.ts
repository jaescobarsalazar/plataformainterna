import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbSearchService } from '@nebular/theme';
import { SmartTableData } from 'app/@core/data/smart-table';
import { LocalDataSource } from 'ng2-smart-table';
import { NbWindowService } from '@nebular/theme';
import { WindowFormComponent } from 'app/pages/modal-overlays/window/window-form/window-form.component';


@Component({
  selector: 'ngx-plantas',
  templateUrl: './plantas.component.html',
  styleUrls: ['./plantas.component.scss']
})
export class PlantasComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  sourceHabitaciones: LocalDataSource = new LocalDataSource();
  sourceSourceSectores: LocalDataSource = new LocalDataSource();
  nombreEmpresa = "";
  idEmpresa = "";
  styleTable=""
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  @ViewChild('disabledEsc', { read: TemplateRef, static: true }) disabledEscTemplate: TemplateRef<HTMLElement>;

  constructor(private service: SmartTableData, private searchService: NbSearchService,
    private route: ActivatedRoute,
    private windowService: NbWindowService) {
    const data = this.service.getData();
    this.source.load(this.datosPruebaPlantas);
    this.sourceHabitaciones.load(this.datosHabitaciones);
    this.sourceSourceSectores.load(this.datosSectores);
    this.route.queryParams.subscribe(params => {
      this.nombreEmpresa = params["nombreEmpresa"];
      this.idEmpresa = params["idEmpresa"];
    });
  }

  gameInstance: any;
  progress = 0;
  isReady = false;



  onDeleteConfirm(){
    console.log("here")
  }
  openWindowForm() {
    this.windowService.open(WindowFormComponent, { title: `Window` });
  }
  ngOnInit() {

  }
   UnityProgress(unityInstance, progress) {
    if (!unityInstance.Module)
      return;
    if (!unityInstance.logo) {
      unityInstance.logo = document.createElement("div");
      unityInstance.logo.className = "logo " + unityInstance.Module.splashScreenStyle;
      unityInstance.container.appendChild(unityInstance.logo);
    }
    if (!unityInstance.progress) {
      unityInstance.progress = document.createElement("div");
      unityInstance.progress.className = "progress " + unityInstance.Module.splashScreenStyle;
      unityInstance.progress.empty = document.createElement("div");
      unityInstance.progress.empty.className = "empty";
      unityInstance.progress.appendChild(unityInstance.progress.empty);
      unityInstance.progress.full = document.createElement("div");
      unityInstance.progress.full.className = "full";
      unityInstance.progress.appendChild(unityInstance.progress.full);
      unityInstance.container.appendChild(unityInstance.progress);
    }
    unityInstance.progress.full.style.width = (100 * progress) + "%";
    unityInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
    if (progress == 1)
      unityInstance.logo.style.display = unityInstance.progress.style.display = "none";
  }
  funct(){
    console.log("jere")
  }
  cargarPlano(event){
    console.log("AQUI")
    this.windowService.open(
      this.disabledEscTemplate,
      {
        title: 'Window without backdrop',
        hasBackdrop: false,
        closeOnEsc: false,
      },
    );
  }

  configuracionSectores = {
    mode: 'internal',
    actions: {
      edit: true,
      add: true,
      delete: true
    },
    sort: false,
    filter: false,
    editable: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nombre: {
        title: 'Nombre',
        type: 'string',
      },
      largo: {
        title: 'Largo',
        type: 'string',
      },
      ancho: {
        title: 'Ancho',
        type: 'string',
      },
      planta: {
        title: 'Sector',
        type: 'string',
      },

    }
  }
  configuracionHabitaciones = {
    mode: 'internal',
    actions: {
      edit: true,
      add: true,
      delete: true
    },
    sort: false,
    filter: false,
    editable: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nombre: {
        title: 'Nombre',
        type: 'string',
      },
      largo: {
        title: 'Largo',
        type: 'string',
      },
      ancho: {
        title: 'Ancho',
        type: 'string',
      },
      sector: {
        title: 'Sector',
        type: 'string',
      },

    }
  }
  configuracionPlantas = {
    mode: 'internal',
    actions: {
      edit: true,
      add: true,
      delete: true
    },
    sort: false,
    filter: false,
    editable: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nombre: {
        title: 'Nombre',
        type: 'string',
      },
      largo: {
        title: 'Largo',
        type: 'string',
      },
      ancho: {
        title: 'Ancho',
        type: 'string',
      }

    }
  }
  datosPruebaPlantas = [
    {
      nombre: "1A",
      largo: "1000M",
      ancho: "1000M",
      superficie: "1000000"
    },
    {
      nombre: "-1A",
      largo: "1000M",
      ancho: "1000M",
      superficie: "1000000"
    },
    {
      nombre: "2A",
      largo: "1000M",
      ancho: "1000M",
      superficie: "1000000"
    },

  ]
  datosSectores = [
    {
      nombre: "Calderas",
      largo: "1000M",
      ancho: "1000M",
      superficie: "1000000",
      planta: "1A",
    },
    {
      nombre: "Bodega",
      largo: "1000M",
      ancho: "1000M",
      superficie: "1000000",
      planta: "-1A",
    },
    {
      nombre: "Patio",
      largo: "1000M",
      ancho: "1000M",
      planta: "1A",
      superficie: "1000000",
    },

  ]
  datosHabitaciones = [
    {
      nombre: "Caldera Principal",
      largo: "1000M",
      ancho: "1000M",
      superficie: "100000",
      sector: "calderas",
    },
    {
      nombre: "Bodega",
      largo: "1000M",
      ancho: "1000M",
      superficie: "100000",
      sector: "Patio",
    },
    {
      nombre: "Bodega Combustible",
      largo: "1000M",
      ancho: "1000M",
      superficie: "100000",
      sector: "Bodega",
    },

  ]
}
