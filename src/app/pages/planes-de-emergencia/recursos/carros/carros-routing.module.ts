import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { CarrosComponent} from './carros.component';


const routes: Routes = [ {
    path: '',
    component: CarrosComponent,
    children: [
      {
        path: '',
        component: ListarComponent
      },
      {
        path: 'agregar',
        component: AgregarComponent
      }]
    }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarrosRoutingModule { }
