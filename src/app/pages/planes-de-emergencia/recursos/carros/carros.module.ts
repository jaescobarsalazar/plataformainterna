import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';
import { CarrosRoutingModule } from './carros-routing.module';
import { CarrosComponent } from './carros.component';

@NgModule({
  declarations: [CarrosComponent,AgregarComponent, ListarComponent],
  imports: [
    CommonModule,
    CarrosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  ]
})
export class CarrosModule { }
