import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtintoresRoutingModule } from './extintores-routing.module';
import { ExtintoresComponent } from './extintores.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [ExtintoresComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    ExtintoresRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  ]
})
export class ExtintoresModule { }
