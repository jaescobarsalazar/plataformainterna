import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';

const routes: Routes = [

  {
    path: 'listar',
    component: ListarComponent
  },
  {
    path: '',
    component: ListarComponent
  },
  {
    path: 'agregar',
    component: AgregarComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViasRoutingModule { }
