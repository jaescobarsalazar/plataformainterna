import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecursosComponent } from './recursos.component';

const routes: Routes = [{
  path: '',
  component: RecursosComponent,
  children: [
    {
      path: 'extintores',
      loadChildren: () => import('./extintores/extintores.module')
        .then(m => m.ExtintoresModule)
    },
    {
      path: 'redes',
      loadChildren: () => import('./extintores/extintores.module')
        .then(m => m.ExtintoresModule)
    },
    {
      path: 'carros',
      loadChildren: () => import('./carros/carros.module')
        .then(m => m.CarrosModule)
    },
  ],
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecursosRoutingModule { }
