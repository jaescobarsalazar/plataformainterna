import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RedesRoutingModule } from './redes-routing.module';
import { RedesComponent } from './redes.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { NbStepperModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RedesComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    RedesRoutingModule,
    NbStepperModule,
    FormsModule,
     ReactiveFormsModule
  ]
})
export class RedesModule { }
