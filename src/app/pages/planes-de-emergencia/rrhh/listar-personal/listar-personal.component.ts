import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbSearchService } from '@nebular/theme';
import { SmartTableData } from 'app/@core/data/smart-table';
import { Trabajador } from 'app/modelos/planDeEmergencia/trabajadores/trabajador';
import { TrabajadoresService } from 'app/servicios/trabajadores/trabajadores';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-listar-personal',
  templateUrl: './listar-personal.component.html',
  styleUrls: ['./listar-personal.component.scss']
})
export class ListarPersonalComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  nombreEmpresa = "";
  idEmpresa = "";
  rutTrabajador:string;
  trabajadores : Trabajador[];
  constructor(private searchService: NbSearchService,
    private route: ActivatedRoute,
    private router: Router,
    private servicioTrabajadores: TrabajadoresService) {
    this.route.queryParams.subscribe(params => {
      this.nombreEmpresa = params["nombreEmpresa"];
      this.idEmpresa = params["idEmpresa"];
    });
  }
  @Output() voto = new EventEmitter<Trabajador>();
  public cargarDatosFichaMedica(event) {
    this.voto.emit(event);
   this.rutTrabajador= event.data.rut
  }
  onDeleteConfirm(){


  }
  ngOnInit() {
    this.servicioTrabajadores.obtenerTrabajadores().subscribe(datos => {
      this.trabajadores = datos;
    });
  }
}
