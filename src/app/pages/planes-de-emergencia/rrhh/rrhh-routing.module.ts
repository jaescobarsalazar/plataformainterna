import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarPersonalComponent } from './listar-personal/listar-personal.component';
import { RrhhComponent } from './rrhh.component';

const routes: Routes = [{
  path: '',
  component: RrhhComponent,
  children: [
    {
      path: 'listar',
      component: ListarPersonalComponent
    },
    {
      path: '',
      component: ListarPersonalComponent,
    },
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RrhhRoutingModule { }
