import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RrhhRoutingModule } from './rrhh-routing.module';
import { RrhhComponent } from './rrhh.component';
import { AgregarPersonalComponent } from './agregar-personal/agregar-personal.component';
import { ModificarPersonalComponent } from './modificar-personal/modificar-personal.component';
import { NbCardModule, NbRouteTabsetModule, NbTabsetModule } from '@nebular/theme';


@NgModule({
  declarations: [RrhhComponent, AgregarPersonalComponent, ModificarPersonalComponent],
  imports: [
    CommonModule,
    RrhhRoutingModule,
    NbCardModule,
    NbTabsetModule,
     NbRouteTabsetModule
  ]
})
export class RrhhModule { }
