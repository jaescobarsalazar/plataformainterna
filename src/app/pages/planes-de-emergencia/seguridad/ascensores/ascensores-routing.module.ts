import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AscensoresComponent } from './ascensores.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';

const routes: Routes = [ {
  path: '',
  component: AscensoresComponent,
  children: [
    {
      path: '',
      component: ListarComponent
    },
    {
      path: 'agregar',
      component: AgregarComponent
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AscensoresRoutingModule { }
