import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AscensoresComponent } from './ascensores.component';

describe('AscensoresComponent', () => {
  let component: AscensoresComponent;
  let fixture: ComponentFixture<AscensoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AscensoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AscensoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
