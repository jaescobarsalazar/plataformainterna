import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AscensoresRoutingModule } from './ascensores-routing.module';
import { AscensoresComponent } from './ascensores.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [AscensoresComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    AscensoresRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  ]
})
export class AscensoresModule { }
