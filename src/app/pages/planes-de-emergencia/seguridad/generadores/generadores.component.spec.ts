import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneradoresComponent } from './generadores.component';

describe('GeneradoresComponent', () => {
  let component: GeneradoresComponent;
  let fixture: ComponentFixture<GeneradoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneradoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
