import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeneradoresRoutingModule } from './generadores-routing.module';
import { GeneradoresComponent } from './generadores.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';



@NgModule({
  declarations: [GeneradoresComponent, AgregarComponent, ListarComponent],
  imports: [
    CommonModule,
    GeneradoresRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  ]
})
export class GeneradoresModule { }
