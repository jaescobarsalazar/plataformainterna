import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialesPeligrososComponent } from './materiales-peligrosos.component';

describe('MaterialesPeligrososComponent', () => {
  let component: MaterialesPeligrososComponent;
  let fixture: ComponentFixture<MaterialesPeligrososComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialesPeligrososComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialesPeligrososComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
