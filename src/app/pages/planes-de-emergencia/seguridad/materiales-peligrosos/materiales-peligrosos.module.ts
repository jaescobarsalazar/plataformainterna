import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialesPeligrososRoutingModule } from './materiales-peligrosos-routing.module';
import { MaterialesPeligrososComponent } from './materiales-peligrosos.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [MaterialesPeligrososComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    MaterialesPeligrososRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  ]
  
})
export class MaterialesPeligrososModule { }
