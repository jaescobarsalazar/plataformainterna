import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { PerimetroSeguridadComponent } from './perimetro-seguridad.component';


const routes: Routes = [ {
  path: '',
  component: PerimetroSeguridadComponent,
  children: [
    {
      path: '',
      component: ListarComponent
    },
    {
      path: 'agregar',
      component: AgregarComponent
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerimetroSeguridadRoutingModule { }
