import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerimetroSeguridadComponent } from './perimetro-seguridad.component';

describe('PerimetroSeguridadComponent', () => {
  let component: PerimetroSeguridadComponent;
  let fixture: ComponentFixture<PerimetroSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerimetroSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerimetroSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
