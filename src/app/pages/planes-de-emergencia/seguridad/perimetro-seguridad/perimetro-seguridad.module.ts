import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerimetroSeguridadRoutingModule } from './perimetro-seguridad-routing.module';
import { PerimetroSeguridadComponent } from './perimetro-seguridad.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [PerimetroSeguridadComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    PerimetroSeguridadRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule  ]
})
export class PerimetroSeguridadModule { }
