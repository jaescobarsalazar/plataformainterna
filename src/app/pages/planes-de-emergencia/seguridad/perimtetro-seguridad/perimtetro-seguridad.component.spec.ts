import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerimtetroSeguridadComponent } from './perimtetro-seguridad.component';

describe('PerimtetroSeguridadComponent', () => {
  let component: PerimtetroSeguridadComponent;
  let fixture: ComponentFixture<PerimtetroSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerimtetroSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerimtetroSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
