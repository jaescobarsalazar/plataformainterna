import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerimtetroSeguridadRoutingModule } from './perimtetro-seguridad-routing.module';
import { PerimtetroSeguridadComponent } from './perimtetro-seguridad.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';


@NgModule({
  declarations: [PerimtetroSeguridadComponent,AgregarComponent,ListarComponent],
  imports: [
    CommonModule,
    PerimtetroSeguridadRoutingModule
  ]
})
export class PerimtetroSeguridadModule { }
