import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'ascensores',
    loadChildren: () => import('./ascensores/ascensores.module')
      .then(m => m.AscensoresModule)
  },
  {
    path: 'generadores',
    loadChildren: () => import('./generadores/generadores.module')
      .then(m => m.GeneradoresModule)
  },
  {
    path: 'materiales-peligrosos',
    loadChildren: () => import('./materiales-peligrosos/materiales-peligrosos.module')
      .then(m => m.MaterialesPeligrososModule)
  },
  {
    path: 'tablero-electrico',
    loadChildren: () => import('./tablero-electrico/tablero-electrico.module')
      .then(m => m.TableroElectricoModule )
  },
  {
    path: 'perimetro-seguridad',
    loadChildren: () => import('./perimetro-seguridad/perimetro-seguridad.module')
      .then(m => m.PerimetroSeguridadModule )
  },
  {
    path: 'vias',
    loadChildren: () => import('./vias/vias.module')
      .then(m => m.ViasModule )
  },
  {
    path: 'zonas-generador',
    loadChildren: () => import('./zonas-generador/zonas-generador.module')
      .then(m => m.ZonasGeneradorModule )
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeguridadRoutingModule { }
