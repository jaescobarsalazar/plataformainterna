import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableroElectricoComponent } from './tablero-electrico.component';

describe('TableroElectricoComponent', () => {
  let component: TableroElectricoComponent;
  let fixture: ComponentFixture<TableroElectricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableroElectricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableroElectricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
