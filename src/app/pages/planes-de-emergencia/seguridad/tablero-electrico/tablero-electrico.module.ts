import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableroElectricoRoutingModule } from './tablero-electrico-routing.module';
import { TableroElectricoComponent } from './tablero-electrico.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [TableroElectricoComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    TableroElectricoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
  ]
})
export class TableroElectricoModule { }
