import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViasRoutingModule } from './vias-routing.module';
import { ViasComponent } from './vias.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [ViasComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    ViasRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule
    
  ]
})
export class ViasModule { }
