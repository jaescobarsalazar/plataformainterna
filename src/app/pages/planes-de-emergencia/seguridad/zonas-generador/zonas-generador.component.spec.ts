import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonasGeneradorComponent } from './zonas-generador.component';

describe('ZonasGeneradorComponent', () => {
  let component: ZonasGeneradorComponent;
  let fixture: ComponentFixture<ZonasGeneradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonasGeneradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonasGeneradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
