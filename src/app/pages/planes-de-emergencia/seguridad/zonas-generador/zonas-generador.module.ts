import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZonasGeneradorRoutingModule } from './zonas-generador-routing.module';
import { ZonasGeneradorComponent } from './zonas-generador.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbInputModule, NbSelectModule, NbStepperComponent, NbStepperModule } from '@nebular/theme';


@NgModule({
  declarations: [ZonasGeneradorComponent, ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    ZonasGeneradorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbInputModule  ]
  
})
export class ZonasGeneradorModule { }
