import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
import { ResumenComponent } from './resumen.component';
const routes: Routes = [{
  path: '',
  component: ResumenComponent,
  children: [
    {
      path: 'principal',
      component: PanelPrincipalComponent
    },
    {
      path: '',
      component: PanelPrincipalComponent,
    },
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResumenRoutingModule { }
