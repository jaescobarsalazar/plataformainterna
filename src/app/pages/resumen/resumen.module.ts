import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResumenRoutingModule } from './resumen-routing.module';
import { ResumenComponent } from './resumen.component';
import { PanelPrincipalComponent } from './panel-principal/panel-principal.component';
import { NbCardComponent, NbCardModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [ResumenComponent,PanelPrincipalComponent],
  imports: [
    ResumenRoutingModule,
    NbCardModule
  ]
})
export class ResumenModule { }
