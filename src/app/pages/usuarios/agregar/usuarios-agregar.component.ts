
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'app/modelos/usuarios/usuarios';
import { UsuariosService } from 'app/servicios/usuarios/usuarios.services';

@Component({
  selector: 'usuarios-agregar',
  styleUrls: ['./usuarios-agregar.component.scss'],
  templateUrl: './usuarios-agregar.component.html',
})
export class UsuariosAgregarComponent {
  formulario: FormGroup
  usuarios: Usuario={
    nombre:"",
    direccion:"",
    rut:"",
    ciudad:"",
    email:"",
    descripcion:"",
    cuartel:"",
    
  } ;
  constructor(private fb: FormBuilder,
    public usuariosServices: UsuariosService,
    private router: Router) { }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: new FormControl(''),
      direccion: new FormControl(''),
      rut: new FormControl(''),
      ciudad: new FormControl(''),
      fono: new FormControl(''),
      email: new FormControl(''),
      descripcion: new FormControl(''),
      cuartel: new FormControl('')
    })
  }
resetForm(){
  this.formulario.reset(this.formulario.value);
}
  doSomething(){
    this.usuarios.nombre=this.formulario.controls.nombre.value;
    this.usuarios.direccion=this.formulario.controls.direccion.value;
    this.usuarios.rut=this.formulario.controls.rut.value;
    this.usuarios.ciudad=this.formulario.controls.ciudad.value;
    this.usuarios.email=this.formulario.controls.email.value;
    this.usuarios.descripcion=this.formulario.controls.descripcion.value;
    this.usuarios.cuartel=this.formulario.controls.cuartel.value;
    this.usuariosServices.agregarUsuarios(this.usuarios);
    this.router.navigate(['pages/usuarios']);
}
 

}
