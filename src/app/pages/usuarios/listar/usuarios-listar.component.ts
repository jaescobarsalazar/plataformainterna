import { Component } from '@angular/core';
import { SmartTableData } from 'app/@core/data/smart-table';
import { LocalDataSource } from 'ng2-smart-table';
import { NbSearchService } from '@nebular/theme';
@Component({
  selector: 'usuarios-listar',
  styleUrls: ['./usuarios-listar.component.scss'],
  templateUrl: './usuarios-listar.component.html',
})
export class UsuariosListarComponent {

  settings = {
    mode: 'internal',
    actions: {
      edit: true,
      add: true,
      delete: true
    },
    sort:false,
    filter:false,
    editable: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      nombre: {
        title: 'Nombre',
        type: 'string',
      },
      rut: {
        title: 'Rut',
        type: 'string',
      },
      rol: {
        title: 'Rol',
        type: 'string',
      },
    }
  }
  datosPrueba = [
    {
      id: 10,
      nombre: "Prueba",
      rut: "18.492.316-5",
      email:"prueba1@pladem.cl",
      rol: "OperadorInterno"
    },
    {
      id: 10,
      nombre: "test",
      rut: "18.492.316-5",
      email:"prueba2@pladem.cl",
      rol: "Ad"
    }
  ]
  source: LocalDataSource = new LocalDataSource();
divTabla="col-12"
divAgregar="col-4"
styleAgregar="visibility:hidden;"
  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(this.datosPrueba);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  cargarAgregar(){
    this.divTabla="col-8" 
    this.styleAgregar='';
  }
  cancelarAgregar(){
    this.divTabla="col-12" 
    this.styleAgregar="visibility:hidden;";
  }
}
