import { Component } from '@angular/core';
import { SmartTableData } from 'app/@core/data/smart-table';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'usuarios-roles',
  styleUrls: ['./usuarios-roles.component.scss'],
  templateUrl: './usuarios-roles.component.html',
})
export class UsuariosRolesComponent {

  settings = {
    mode: 'external',
    actions: {
      edit: false,
      add: false,
      delete: false
    },
    editable: false,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      nombre: {
        title: 'nombre',
        type: 'number',
      },
      usuarios: {
        title: 'usuarios',
        type: 'string',
      },
      empresas: {
        title: 'empresas',
        type: 'string',
      },
      soporte: {
        title: 'soporte',
        type: 'string',
      },
      planos: {
        title: 'planos',
        type: 'string',
      },
    }
  }
  datosPrueba = [
    {
      nombre: 'InternoBasico',
      usuarios: 'true',
      empresas: 'false',
      soporte: 'false',
      planos: 'true'
    },
    {
      nombre: 'InternoAvanzado',
      usuarios: 'true',
      empresas: 'true',
      soporte: 'false',
      planos: 'true'
    },
    {
      nombre: 'Admin',
      usuarios: 'true',
      empresas: 'true',
      soporte: 'true',
      planos: 'true'
    },
  ]
  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(this.datosPrueba);
    console.log(data)
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
