import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsuariosComponent } from './usuarios.component';
import { UsuariosListarComponent } from './listar/usuarios-listar.component';
import { UsuariosAgregarComponent } from './agregar/usuarios-agregar.component';
import { UsuariosRolesComponent } from './roles/usuarios-roles.component';

const routes: Routes = [
  {
    path: '',
    component: UsuariosComponent,
    children: [
      {
        path: 'listar',
        component: UsuariosListarComponent,
      },
      {
        path: '',
        component: UsuariosListarComponent,
      },
      {
        path: 'agregar',
        component: UsuariosAgregarComponent,
      },
      {
        path: 'roles',
        component: UsuariosRolesComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class UsuariosRouting {
}

