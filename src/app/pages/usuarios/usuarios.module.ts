import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbRouteTabsetModule,
  NbSelectModule,
  NbTabsetModule,
  NbUserModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { UsuariosRouting } from './usuarios-routing';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { UsuariosComponent } from './usuarios.component';
import { UsuariosListarComponent } from './listar/usuarios-listar.component';
import { UsuariosAgregarComponent } from './agregar/usuarios-agregar.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UsuariosRolesComponent } from './roles/usuarios-roles.component';

@NgModule({
  imports: [
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    UsuariosRouting,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    Ng2SmartTableModule,
    NbTabsetModule,
    NbRouteTabsetModule
  ],
  declarations: [
    UsuariosComponent,
    UsuariosListarComponent,
    UsuariosAgregarComponent,
    UsuariosRolesComponent
  ],
})
export class UsuariosModule { }
