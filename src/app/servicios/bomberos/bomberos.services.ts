import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Bombero } from 'app/modelos/bomberos/bomberos';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BomberosService {

  listadoBomberos: AngularFirestoreCollection<Bombero>;
  bomberos: Observable<Bombero[]>;
  bomberoDoc: AngularFirestoreDocument<Bombero>;

  constructor(public db: AngularFirestore) {
    this.listadoBomberos = this.db.collection('bomberos');
    this.bomberos = this.listadoBomberos.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Bombero;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerBomberos() {
    return this.bomberos;
  }

  agregarBombero(bombero: Bombero) {
    this.listadoBomberos.add(bombero);
  }

  borrarBombero(bombero: Bombero) {
    this.bomberoDoc = this.db.doc(`bomberos/${bombero.id}`);
    this.bomberoDoc.delete();
  }

  actualizarbombero(bombero: Bombero) {
    this.bomberoDoc = this.db.doc(`bomberos/${bombero.id}`);
    this.bomberoDoc.update(bombero);
  }

}