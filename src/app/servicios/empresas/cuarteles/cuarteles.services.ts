import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Cuartel } from '../../../modelos/cuarteles/cuartel';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CuartelesService {

  listadoCuarteles: AngularFirestoreCollection<Cuartel>;
  cuarteles: Observable<Cuartel[]>;
  cuartelDoc: AngularFirestoreDocument<Cuartel>;

  constructor(public db: AngularFirestore) {
    this.listadoCuarteles = this.db.collection('cuarteles');
    this.cuarteles = this.listadoCuarteles.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Cuartel;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerCuarteles() {
    return this.cuarteles;
  }

  agregarCuartel(cuartel: Cuartel) {
    this.listadoCuarteles.add(cuartel);
  }

  borrarCuartel(cuartel: Cuartel) {
    this.cuartelDoc = this.db.doc(`cuarteles/${cuartel.id}`);
    this.cuartelDoc.delete();
  }

  actualizarCuartel(cuartel: Cuartel) {
    this.cuartelDoc = this.db.doc(`cuarteles/${cuartel.id}`);
    this.cuartelDoc.update(cuartel);
  }

}