import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Empresa } from '../../modelos/empresas/empresas';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {

  listadoEmpresas: AngularFirestoreCollection<Empresa>;
  empresas: Observable<Empresa[]>;
  empresaDoc: AngularFirestoreDocument<Empresa>;

  constructor(public db: AngularFirestore) {
    this.listadoEmpresas = this.db.collection('empresas');
    this.empresas = this.listadoEmpresas.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Empresa;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerEmpresas() {
    return this.empresas;
  }

  agregarEmpresa(empresa: Empresa) {
    this.listadoEmpresas.add(empresa);
  }

  borrarEmpresa(empresa: Empresa) {
    this.empresaDoc = this.db.doc(`empresas/${empresa.id}`);
    this.empresaDoc.delete();
  }

  actualizarempresa(empresa: Empresa) {
    this.empresaDoc = this.db.doc(`empresas/${empresa.id}`);
    this.empresaDoc.update(empresa);
  }

}