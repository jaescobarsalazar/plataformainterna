import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { FichaMedica } from "app/modelos/planDeEmergencia/fichaMedica/fichaMedica";

@Injectable({
  providedIn: "root",
})
export class FichaMedicaService {
  listadoFichasMedicas: AngularFirestoreCollection<FichaMedica>;
  fichasMedicas: Observable<FichaMedica[]>;
  fichaMedicaDoc: AngularFirestoreDocument<FichaMedica>;
  rutaDatos: string = "fichasMedicas";
  constructor(public db: AngularFirestore) {
    this.listadoFichasMedicas = this.db.collection(this.rutaDatos);
    this.fichasMedicas = this.listadoFichasMedicas.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data() as FichaMedica;
          data.Id = a.payload.doc.id;
          return data;
        });
      })
    );
  }

  obtenerFichasMedicas() {
    return this.fichasMedicas;
  }

  agregarFichaMedica(fichaMedica: FichaMedica) {
    this.listadoFichasMedicas.add(fichaMedica);
  }

  borrarFichaMedica(fichaMedica: FichaMedica) {
    this.fichaMedicaDoc = this.db.doc(`fichasMedicas/${fichaMedica.Id}`);
    this.fichaMedicaDoc.delete();
  }

  actualizarFichaMedica(fichaMedica: FichaMedica) {
    this.fichaMedicaDoc = this.db.doc(`fichasMedicas/${fichaMedica.Id}`);
    this.fichaMedicaDoc.update(fichaMedica);
  }
}
