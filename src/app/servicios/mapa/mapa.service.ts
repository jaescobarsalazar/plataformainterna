import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Planta,Sector,Ubicacion } from 'app/modelos/mapa/mapa';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MapaService {

  listadoPlantas: AngularFirestoreCollection<Planta>;
  listadoSectores: AngularFirestoreCollection<Sector>;
  listadoUbicaciones: AngularFirestoreCollection<Ubicacion>;
  plantas: Observable<Planta[]>;
  sectores: Observable<Sector[]>;
  ubicaciones: Observable<Ubicacion[]>;
  redDocPlanta: AngularFirestoreDocument<Planta>;
  redDocSector: AngularFirestoreDocument<Sector>;
  redDocUbicacion: AngularFirestoreDocument<Ubicacion>;

  constructor(public db: AngularFirestore) {
    this.listadoPlantas = this.db.collection('plantas');
    this.listadoSectores = this.db.collection('sectores');
    this.listadoUbicaciones = this.db.collection('ubicaciones');
    //sincronizar [Plantas]
    this.plantas = this.listadoPlantas.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Planta;
        data.id = a.payload.doc.id;
         return data;
      });

    }));

      //sincronizar [Sectores]
      this.sectores = this.listadoSectores.snapshotChanges().pipe(map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Sector;
          data.id = a.payload.doc.id;
          console.log(data)
           return data;
        });
      }));

  //sincronizar [Ubicaciones]
        this.ubicaciones = this.listadoUbicaciones.snapshotChanges().pipe(map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as Ubicacion;
            data.id = a.payload.doc.id;
             return data;
          });
        }));

  }

  obtenerPlantas() {
    return this.plantas;
  }

  obtenerSectores() {
    return this.sectores;
  }

  obtenerUbicaciones() {
    return this.ubicaciones;
  }

  agregarPlantas(plantas: Planta) {
    this.listadoPlantas.add(plantas);
  }
  agregarsectores(sectores: Sector) {
    this.listadoSectores.add(sectores);
  }
  agregarUbicaciones(ubicaciones: Ubicacion) {
    this.listadoUbicaciones.add(ubicaciones);
  }

  borrarPlanta(planta: Planta) {
    this.redDocPlanta = this.db.doc(`plantas/${planta.id}`);
    this.redDocPlanta.delete();
  }
  borrarSector(sector: Sector) {
    this.redDocSector = this.db.doc(`sectores/${sector.id}`);
    this.redDocSector.delete();
  }
  borrarRed(ubicacion: Ubicacion) {
    this.redDocUbicacion = this.db.doc(`ubicaciones/${ubicacion.id}`);
    this.redDocUbicacion.delete();
  }



}
