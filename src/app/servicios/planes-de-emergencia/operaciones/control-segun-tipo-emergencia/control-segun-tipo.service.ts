import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Red } from 'app/modelos/redes/redes';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RedesService {

  listadoRedes: AngularFirestoreCollection<Red>;
  redes: Observable<Red[]>;
  redDoc: AngularFirestoreDocument<Red>;

  constructor(public db: AngularFirestore) {
    this.listadoRedes = this.db.collection('redes');
    this.redes = this.listadoRedes.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Red;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerRedes() {
    return this.redes;
  }

  agregarRed(red: Red) {
    this.listadoRedes.add(red);
  }

  borrarRed(red: Red) {
    this.redDoc = this.db.doc(`redes/${red.id}`);
    this.redDoc.delete();
  }

  actualizarRed(red: Red) {
    this.redDoc = this.db.doc(`redes/${red.id}`);
    this.redDoc.update(red);
  }

}
