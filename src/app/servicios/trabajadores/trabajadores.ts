import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from "@angular/fire/firestore";
import { Trabajador } from "app/modelos/planDeEmergencia/trabajadores/trabajador";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class TrabajadoresService {
  listadoTrabajadores: AngularFirestoreCollection<Trabajador>;
  trabajadores: Observable<Trabajador[]>;
  trabajadorDoc: AngularFirestoreDocument<Trabajador>;
  trabajdoresFiltrados: Observable<Trabajador[]>;
  trabajador2: Trabajador;
  constructor(public db: AngularFirestore) {
    this.listadoTrabajadores = this.db.collection("trabajadores");
    this.trabajadores = this.listadoTrabajadores.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data() as Trabajador;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }

  obtenerTrabajadores() {

   /**  this.trabajdoresFiltrados = this.trabajadores.filter(trabaja=> {
      trabaja.filter
      trabaja.forEach(test=>{
        console.log(test)
      })
      console.log(trabaja);
      return true
    });
    */
    return this.trabajdoresFiltrados;
  }

  agregarTrabajador(trabajador: Trabajador) {
    this.listadoTrabajadores.add(trabajador);
  }

  borrarTrabajador(trabajador: Trabajador) {
    this.trabajadorDoc = this.db.doc(`trabajadores/${trabajador.id}`);
    this.trabajadorDoc.delete();
  }

  actualizarempresa(trabajador: Trabajador) {
    this.trabajadorDoc = this.db.doc(`trabajadores/${trabajador.id}`);
    this.trabajadorDoc.update(trabajador);
  }
}
