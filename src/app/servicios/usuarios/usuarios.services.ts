import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Usuario } from 'app/modelos/usuarios/usuarios';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  listadoUsuarios: AngularFirestoreCollection<Usuario>;
  usuarios: Observable<Usuario[]>;
  usuarioDoc: AngularFirestoreDocument<Usuario>;

  constructor(public db: AngularFirestore) {
    this.listadoUsuarios = this.db.collection('usuarios');
    this.usuarios = this.listadoUsuarios.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Usuario;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerUsuarios() {
    return this.usuarios;
  }

  agregarUsuarios(usuario: Usuario) {
    this.listadoUsuarios.add(usuario);
  }

  borrarUsuarios(usuario: Usuario) {
    this.usuarioDoc = this.db.doc(`usuarios/${usuario.id}`);
    this.usuarioDoc.delete();
  }

  actualizarUsuario(usuario: Usuario) {
    this.usuarioDoc = this.db.doc(`usuarios/${usuario.id}`);
    this.usuarioDoc.update(usuario);
  }

}