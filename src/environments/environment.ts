/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBkABtI4GfCRDvnbtL8B3hg09TQcD8hBZE",
    authDomain: "bomberos-b75cd.firebaseapp.com",
    databaseURL: "https://bomberos-b75cd.firebaseio.com",
    projectId: "bomberos-b75cd",
    storageBucket: "bomberos-b75cd.appspot.com",
    messagingSenderId: "753613459388",
    appId: "1:753613459388:web:758064f93063508280e5b9"
  }
};
